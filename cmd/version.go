package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

const version = "0.1"

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Version info",
	Long:  "Software and driver versions",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Application: v", version)
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
