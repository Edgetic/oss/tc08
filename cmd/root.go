package cmd

import (
	"errors"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	homedir "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/edgetic/tools/tc08/pkg/picotech"
)

var (
	// Used for flags.
	cfgFile     string
	userLicense string
	channels    string

	rootCmd = &cobra.Command{
		Use:   "tc08",
		Short: "Utility for running the TC-08",
		Long:  `A CLI utiliity for interacting with Picotech's TC-08 data loggers`,
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			if viper.GetBool("debug") {
				log.SetLevel(log.DebugLevel)
			}
		},
		Run: func(cmd *cobra.Command, args []string) {
			//Config Stuff
			deviceCfg := make(map[string]picotech.DeviceConfig)
			err := viper.UnmarshalKey("devices", &deviceCfg)
			if err != nil {
				log.Error("Config Error:", err.Error())
				return
			}

			// Allow a global interval override
			var globalFreq time.Duration
			err = viper.UnmarshalKey("globalFrequency", &globalFreq)
			if err != nil {
				log.Fatal("Invalid global frequency: ", err)
			}

			devices, err := startLogging(deviceCfg, globalFreq)
			if err != nil {
				log.Fatal(err.Error())
			}

			//Set all the devices to close
			for _, d := range devices {
				defer d.CloseDevice()
			}

			measurements := mergeDevices(devices)

			// Handle closing
			sigc := make(chan os.Signal)

			signal.Notify(sigc,
				syscall.SIGHUP,
				syscall.SIGINT,
				syscall.SIGTERM,
				syscall.SIGQUIT)

			for {
				select {
				case <-sigc:
					return
				case m := <-measurements:
					log.Info(m.Measurement.Time.Format(time.RFC3339), ",", m.Device, ",", m.Measurement.Channel, ",", m.Measurement.Value)
				}
			}

		},
	}
)

type IDedMeasurement struct {
	Device      string
	Measurement picotech.Measurement
}

// Open the devices and start them in streaming mode, common for all operational modes
func startLogging(cfg map[string]picotech.DeviceConfig, intervalOverride time.Duration) ([]*picotech.USBPicoDevice, error) {
	devices, err := picotech.OpenDevices(cfg)
	if err != nil {
		return devices, err
	}

	var interval time.Duration

	for _, d := range devices {
		log.WithFields(
			log.Fields{
				"device": d.ID,
			}).Info("Starting stream mode")

		// Set measuremnt frequency
		if intervalOverride == 0 {
			// Local interval
			if freq := cfg[strings.ToLower(d.ID)].Frequency; freq == 0 {
				//Default
				err = viper.UnmarshalKey("defaultFrequency", &interval)
				if err != nil {
					return devices, errors.New("Tried to use default frequency and failed")
				}
			} else {
				interval = freq
			}
		} else {
			interval = intervalOverride
		}

		log.Debug("Requested interval ", interval)

		_, err = d.StartLogging(interval)
		if err != nil {
			log.WithFields(
				log.Fields{
					"device": d.ID,
				}).Errorf("Could not start device: %s", err.Error())
			d.CloseDevice()
			continue
		}
	}

	return devices, nil
}

// merge the output channels of the devices
func mergeDevices(devices []*picotech.USBPicoDevice) <-chan IDedMeasurement {
	out := make(chan IDedMeasurement)

	for _, d := range devices {
		go func(dev *picotech.USBPicoDevice) {
			for m := range dev.Update {
				out <- IDedMeasurement{dev.ID, m}
			}
		}(d)
	}

	return out
}

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.cobra.yaml)")
	rootCmd.PersistentFlags().BoolP("debug", "d", false, "Display debug information")
	rootCmd.PersistentFlags().StringP("frequency", "f", "", "Global measurement frequency for all devices e.g. 500ms")

	viper.BindPFlag("debug", rootCmd.PersistentFlags().Lookup("debug"))
	viper.BindPFlag("globalFrequency", rootCmd.PersistentFlags().Lookup("frequency"))

	viper.SetDefault("globalFrequency", time.Duration(0))
	viper.SetDefault("defaultFrequency", time.Duration(1)*time.Second)

	// Output to stdout instead of the default stderr
	log.SetOutput(os.Stdout)

	//Default log level
	log.SetLevel(log.InfoLevel)
}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			log.Fatal(err.Error())
		}

		// Search config in home directory with name ".tc08" (without extension).
		viper.AddConfigPath(home)
		viper.AddConfigPath("config/")
		viper.SetConfigName(".tc08")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		log.Info("Using config file:", viper.ConfigFileUsed())
	}
}

func defaults() {

}
