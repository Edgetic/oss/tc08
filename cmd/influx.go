package cmd

import (
	"errors"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	client "github.com/influxdata/influxdb1-client/v2"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/edgetic/tools/tc08/pkg/picotech"

	log "github.com/sirupsen/logrus"
)

var influxCmd = &cobra.Command{
	Use:   "influx",
	Short: "Log to InfluxDB",
	Long:  "Log TC-08 data to an InfluxDB database.",
	Run: func(cmd *cobra.Command, args []string) {

		//Config Stuff
		deviceCfg := make(map[string]picotech.DeviceConfig)
		err := viper.UnmarshalKey("devices", &deviceCfg)
		if err != nil {
			log.Info("Config Error:", err.Error())
			return
		}

		// Allow a global interval override
		var globalFreq time.Duration
		err = viper.UnmarshalKey("globalFrequency", &globalFreq)
		if err != nil {
			log.Fatal("Invalid global frequency", err)
		}

		devices, err := startLogging(deviceCfg, globalFreq)
		if err != nil {
			log.Fatal(err.Error())
		}

		//Set all the devices to close
		for _, d := range devices {
			defer d.CloseDevice()
		}

		measurements := mergeDevices(devices)

		c, err := client.NewHTTPClient(client.HTTPConfig{
			Addr:     fmt.Sprintf("http://%s:%s", viper.GetString("influx.host"), viper.GetString("influx.port")),
			Username: viper.GetString("influx.user"),
			Password: viper.GetString("influx.password"),
			Timeout:  time.Duration(viper.GetInt("influx.timeout")) * time.Second,
		})
		if err != nil {
			exitError("Error creating clint", err, devices)
		}
		defer c.Close()

		err = createDatabase(c, viper.GetString("influx.database"))
		if err != nil {
			exitError("Error creating database", err, devices)
		}

		// Handle closing
		sigc := make(chan os.Signal)
		signal.Notify(sigc,
			syscall.SIGHUP,
			syscall.SIGINT,
			syscall.SIGTERM,
			syscall.SIGQUIT)

		measurementName := viper.GetString("influx.measurement")
		precision := viper.GetString("influx.precision")
		batchsize := viper.GetInt("influx.batchsize")

		for {
			bp, err := client.NewBatchPoints(client.BatchPointsConfig{
				Precision: precision,
				Database:  viper.GetString("influx.database"),
			})

			if err != nil {
				exitError("Error creating point batch", err, devices)
			}

			// Build a batch
			for i := 0; i < batchsize; i++ {
				select {
				case <-sigc:
					//Exit
					c.Write(bp)
					return
				case m := <-measurements:
					pt, err := client.NewPoint(
						measurementName,
						map[string]string{
							"device":  m.Device,
							"channel": m.Measurement.Channel,
						},
						map[string]interface{}{
							"value": m.Measurement.Value,
						},
						m.Measurement.Time)

					if err != nil {
						log.Error("Error creating point:", err.Error())
						continue
					}
					bp.AddPoint(pt)
				}
			}
			log.Debugf("Writing %d points to influx", len(bp.Points()))
			err = c.Write(bp)
			if err != nil {
				log.Error("Error writing to db:", err.Error())
			}
		}
	},
}

func createDatabase(c client.Client, dbName string) error {
	q := client.NewQuery(fmt.Sprintf("CREATE DATABASE \"%s\"", dbName), "", "")
	if response, err := c.Query(q); err != nil || response.Error() != nil {
		if err != nil {
			return err
		}

		if response != nil {
			if response.Error() != nil {
				return response.Error()
			}
		} else {
			return errors.New("No response from database")
		}
	}
	return nil
}

// Exits with error
func exitError(msg string, err error, devices []*picotech.USBPicoDevice) {
	// Make sure all deveics are closed first since os.Exit() skips deferred funcs
	for _, d := range devices {
		d.CloseDevice()
	}
	log.Fatal(msg, err.Error())
}

func init() {

	influxCmd.Flags().String("host", "localhost", "Address of the influx database")
	influxCmd.Flags().String("port", "8086", "Port of the influx database")
	influxCmd.Flags().String("database", "tc08", "The influx database to store the data")
	influxCmd.Flags().String("user", "", "Username for influx database")
	influxCmd.Flags().String("password", "", "Password for influx database")

	viper.BindPFlag("influx.host", influxCmd.Flags().Lookup("host"))
	viper.BindPFlag("influx.port", influxCmd.Flags().Lookup("port"))
	viper.BindPFlag("influx.database", influxCmd.Flags().Lookup("database"))
	viper.BindPFlag("influx.user", influxCmd.Flags().Lookup("user"))
	viper.BindPFlag("influx.password", influxCmd.Flags().Lookup("password"))

	viper.SetDefault("influx.measurement", "sensors")
	viper.SetDefault("influx.precision", "ms")
	viper.SetDefault("influx.timeout", 10)
	viper.SetDefault("influx.batchsize", 10)

	rootCmd.AddCommand(influxCmd)
}
