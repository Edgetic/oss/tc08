# tc08 - A utility for the picotech TC-08 datalogger

This CLI utility provides an interface to the Picotech USB TC-08 data logger. It allows for logging thermocouple data to a CSV file.

## Installation

Build the application using `make` (x86_64 or raspberry pi) .

### Driver ###

This utility depends on the Picotech TC-08 driver (__libusbtc08__) being installed. The latest drivers are available on [Picotech's website](https://www.picotech.com/downloads/).

#### Linux #####
On Ubuntu or Raspbian they can be installed via:

```
sudo apt-get install libusbtc08 
```

## Usage

The standard mode of operation prints the measurements to standard out as a log stream with a line per channel per device in the format:

 ```
 INFO[XXXX] {RFC3339_TIMESTAMP},{DEVICE_SERIAL},{CHANNEL},{MEASUREMENT}
 ```
 For example:

```
INFO[0006] 2020-10-19T18:56:54+01:00,AO109/747,ch1,23.410807 
INFO[0006] 2020-10-19T18:56:54+01:00,AO109/747,ch2,22.952028 
INFO[0011] 2020-10-19T18:56:59+01:00,AO109/747,ch1,23.44012 
INFO[0011] 2020-10-19T18:56:59+01:00,AO109/747,ch2,23.018045 
INFO[0016] 2020-10-19T18:57:04+01:00,AO109/747,ch1,23.410143 
INFO[0016] 2020-10-19T18:57:04+01:00,AO109/747,ch2,23.06514 
```
Other modes such as outputting to an Influx Database are also supported, run `tc08 help` for a full list of modes available.

### Stopping Logging

To stop logging, use **CTRL+C**, the utility will ensure the devices are closed correctly.

### Influx Mode

The utility can log measurements to an influx database, configuration for the database can be provided via the config file or CLI flags.

```tc08 influx [flags]```

Database connection parameters like host, port, user etc. are configurable via flags from the command line. Some parameters are only configurable via the config file, defaults shown below.

```yaml
influx:
    # Config w/ Flag override
    host: localhost         # Database host address excluding any "http://"
    port: 8086              # Connection port
    user:                   # Username
    password:               # Auth password
    database: tc08          # The database name
    # Config file only
    measurement: sensors    # The measurement table name
    precision: ms           # Database write precision
    timeout: 10             # Seconds for database connection timeout
    batchsize: 10           # Number of readings to batch into a single POST to the database
```

## Device configuration

A config file (`.tc08.yaml`) is expected in either your home directory ($HOME) or a local folder `./config/`. More than one device can be figured, each device configuration is headed by the device serial number (found on the back of the TC-08). The mode of channels 1 through 8 are configured as individual items in this dictionary. 

```yaml
devices:
    {DEVICE_SERIAL}:                    # Device serial number
        enableCJC: true                 # Log the CJC temperature
        frequency: 500ms                # Individual device measurement frequency
        ch{1-8}:                        # Omit unneeded channels
            mode: {B,E,J,K,N,R,S,T,X}   # Channel mode (thermocouple type), X for voltage reading
            alias: {CHANNEL_NAME}       # Optional name to use instead of "ch{1-8}"
```

## Troubleshooting

**no Picotech USB TC08's found** - Ensure that the TC-08s are not being used by another program. If no other programs are using the TC-08s but the indicator lights on the devices are lit, disconnect the TC-08 and reconnect it to reset. 