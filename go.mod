module gitlab.com/edgetic/tools/tc08

go 1.15

require (
	github.com/influxdata/influxdb1-client v0.0.0-20200827194710-b269163b24ab
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
)
