package picotech

import "time"

// Measurement is an individual measuremetn from the TC-08 logger
type Measurement struct {
	Channel string
	Time    time.Time
	Value   float32
}
