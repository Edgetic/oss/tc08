package picotech

import (
	"time"
	"unicode/utf8"
)

type DeviceConfig struct {
	Channel1  ChannelConfig `mapstructure:"ch1"`
	Channel2  ChannelConfig `mapstructure:"ch2"`
	Channel3  ChannelConfig `mapstructure:"ch3"`
	Channel4  ChannelConfig `mapstructure:"ch4"`
	Channel5  ChannelConfig `mapstructure:"ch5"`
	Channel6  ChannelConfig `mapstructure:"ch6"`
	Channel7  ChannelConfig `mapstructure:"ch7"`
	Channel8  ChannelConfig `mapstructure:"ch8"`
	CJC       bool          `mapstructure:"enableCJC"`
	Frequency time.Duration `mapstructure:"frequency"`
}

type ChannelConfig struct {
	Mode  string `mapstructure:"mode"`
	Alias string `mapstructure:"alias"`
}

func (cfg DeviceConfig) ToArray() []ChannelConfig {
	return []ChannelConfig{
		cfg.Channel1,
		cfg.Channel2,
		cfg.Channel3,
		cfg.Channel4,
		cfg.Channel5,
		cfg.Channel6,
		cfg.Channel7,
		cfg.Channel8,
	}
}

// ToString converts the config to an array of strings to be iterated over
func (cfg DeviceConfig) ToString() string {
	out := ""
	for _, s := range cfg.ToArray() {
		if s.Mode == "" {
			out += " "
		} else {
			out += s.Mode
		}
	}
	return out
}

func (ch ChannelConfig) GetMode() rune {
	if ch.Mode == "" {
		return ' '
	}

	r, _ := utf8.DecodeRuneInString(ch.Mode)
	return r
}
