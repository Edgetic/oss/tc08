package picotech

/*
#cgo LDFLAGS: -lusbtc08
#include <libusbtc08-1.7/TC08Api.h>
#include <stdlib.h>

int usb_tc08_max_channels() {
	return USBTC08_MAX_CHANNELS;
}
*/
import "C"
import (
	"errors"
	"fmt"
	"math"
	"strings"
	"time"
	"unsafe"

	log "github.com/sirupsen/logrus"
)

const TC08MaxChannels int16 = 8

type tc08InfoLine int16

const (
	USBTC08LINE_DRIVER_VERSION tc08InfoLine = iota
	USBTC08LINE_KERNEL_DRIVER_VERSION
	USBTC08LINE_HARDWARE_VERSION
	USBTC08LINE_VARIANT_INFO
	USBTC08LINE_BATCH_AND_SERIAL
	USBTC08LINE_CAL_DATE
)

type tc08MeasurementUnit int16

const (
	centigrade tc08MeasurementUnit = iota
	fahrenheit
	kelvin
	rankine
)

const updateFreq = 3 // Seconds

type USBPicoDevice struct {
	handle         C.short
	intialized     bool
	running        bool
	stopSignal     chan bool
	activeChannels []bool
	Update         chan Measurement
	startTime      time.Time
	overflowTime   time.Time
	ID             string
	channelNames   []string
}

func OpenDevices(deviceCfgs map[string]DeviceConfig) ([]*USBPicoDevice, error) {

	devices := []*USBPicoDevice{}

	// Repeated calls to open_unit gets handles to all connected TC08s
	for h := C.usb_tc08_open_unit(); h != 0; h = C.usb_tc08_open_unit() {
		if h < 0 {
			// An error occured
			return nil, lastDeviceError("Error opening unit", h)
		} else if h == 0 && len(devices) == 0 {
			return nil, errors.New("no Picotech USB TC08's found")
		}

		l := USBPicoDevice{
			handle:         h,
			intialized:     false,
			running:        false,
			stopSignal:     make(chan bool),
			activeChannels: make([]bool, C.usb_tc08_max_channels()+1),
			Update:         make(chan Measurement, 100),
			channelNames:   []string{"CJC", "ch1", "ch2", "ch3", "ch4", "ch5", "ch6", "ch7", "ch8"},
		}

		//Configure the device
		serial, err := l.getSerial()
		if err != nil {
			return devices, err
		}

		l.ID = serial

		if cfg, ok := deviceCfgs[strings.ToLower(serial)]; ok {
			//Device is in the config
			err = l.configChannels(cfg)
			if err != nil {
				return devices, err
			}
		} else {
			log.WithFields(log.Fields{
				"device": l.ID,
			}).Warn("No config found skipping device")
			continue
		}

		devices = append(devices, &l)

	}

	if len(devices) == 0 {
		return devices, errors.New("No Picotech USB TC08's found")
	}

	return devices, nil
}

func (device *USBPicoDevice) getSerial() (string, error) {
	// C-style buffer for string from API
	bufsize := 8192
	ptr := C.CString(strings.Repeat("0", bufsize))
	defer C.free(unsafe.Pointer(ptr))

	// API call, 0 indicates an error, need last error to findo out what it was
	if int16(C.usb_tc08_get_unit_info2(device.handle, (*C.char)(ptr), C.short(bufsize), C.short(USBTC08LINE_BATCH_AND_SERIAL))) == 0 {
		return "", lastDeviceError("error getting unit info", device.handle)
	}

	// GoString converts the buffer into a proper go string, respects c-string endings unlike using C.GoBytes
	return C.GoString((*C.char)(ptr)), nil
}

func (device *USBPicoDevice) configChannels(cfg DeviceConfig) error {
	maxCh := int(C.usb_tc08_max_channels())

	log.WithFields(log.Fields{
		"device": device.ID,
	}).Debug("Applying config: ", cfg)

	if cfg.CJC {
		device.activeChannels[0] = true

		// Apply configuration
		if int16(C.usb_tc08_set_channel(device.handle, C.short(0), C.char('T'))) == 0 {
			// Close the device
			C.usb_tc08_close_unit(device.handle)
			device.intialized = false
			device.activeChannels = make([]bool, C.usb_tc08_max_channels()+1)

			return lastDeviceError("error enabling CJC", device.handle)
		}
	}

	for i, c := range cfg.ToArray() {
		ch := i + 1 // chanels are 1 indexed

		if ch > maxCh {
			// String too long but thats ok
			return nil
		}

		if c.Mode != "" {
			device.activeChannels[ch] = true
		}

		// Apply configuration
		if int16(C.usb_tc08_set_channel(device.handle, C.short(ch), C.char(c.GetMode()))) == 0 {
			// Close the device
			C.usb_tc08_close_unit(device.handle)
			device.intialized = false
			device.activeChannels = make([]bool, C.usb_tc08_max_channels()+1)

			return lastDeviceError(fmt.Sprintf("error setting channel %d", ch), device.handle)
		}

		if c.Alias != "" {
			device.channelNames[ch] = c.Alias
		}
	}
	device.intialized = true
	return nil
}

func lastDeviceError(errString string, handle C.short) error {
	return fmt.Errorf("%s, code : %d", errString, C.usb_tc08_get_last_error(handle))
}

func (device *USBPicoDevice) ChannelActive(ch int) bool {
	if device.intialized && ch < len(device.activeChannels) {
		return device.activeChannels[ch]
	}
	return false
}

func (device *USBPicoDevice) MaxChannels() int {
	return int(C.usb_tc08_max_channels())
}

// StartLogging starts the TC-08 logging measurements from configured channels, measurements are posted into the Update channel
func (device *USBPicoDevice) StartLogging(requestInterval time.Duration) (time.Duration, error) {

	if !device.intialized {
		return 0, errors.New("Device not intialised, call OpenDevice")
	}

	minIntervalms := int32(C.usb_tc08_get_minimum_interval_ms(device.handle))

	if minIntervalms == 0 {
		return 0, lastDeviceError("Error getting minimum interval", device.handle)
	}

	minInterval := time.Duration(minIntervalms) * time.Millisecond

	if minInterval > requestInterval {
		log.Warnf("Specified interval too small, using %s\n", minInterval.String())
		requestInterval = minInterval
	}

	log.WithFields(log.Fields{
		"device": device.ID,
	}).Debug("Starting streaming mode with interval: ", requestInterval)

	// Set the device running.
	// Documentation is wrong, response isn't the actual delay, seems to just be a 1 if there weren't any errors
	response := int(C.usb_tc08_run(device.handle, C.long(requestInterval.Milliseconds())))

	if response == 0 {
		return 0, lastDeviceError("Error starting device", device.handle)
	}

	device.startTime = time.Now()
	device.overflowTime = device.startTime.Add(time.Duration(math.MaxInt32) * time.Millisecond)

	log.WithFields(
		log.Fields{
			"device": device.ID,
		}).Debug("Log start time ", device.startTime)
	log.WithFields(
		log.Fields{
			"device": device.ID,
		}).Debug("Expected device time overflow ", device.overflowTime)

	device.running = true

	go device.streamRead(requestInterval)

	return requestInterval, nil
}

func (device *USBPicoDevice) streamRead(delay time.Duration) {
	buffSize := 100
	tempBuf := make([]float32, buffSize)
	overflow := C.short(0)

	ptrTemp := unsafe.Pointer(&tempBuf[0])

	ticker := time.NewTicker(delay / 2) // At most 1/2 interval out of date

	for {
		select {
		case <-device.stopSignal:
			// Stop logging
			C.usb_tc08_stop(device.handle)
			device.running = false
			device.stopSignal <- true
			return
		case <-ticker.C:
			for ch, active := range device.activeChannels {
				if active {
					// ------   Potential overflow error here  -----------
					// Time is relative to start and returned as milliseconds in an int32 (2**32 milliseconds is ~50 days)
					// It is undocumented what the tc-08 does after 50 days of logging. For now, assuming it will overflow
					// so we'll adjust the reference start time if logging is longer than 2**32 milliseconds
					nvals := int(C.usb_tc08_get_temp(device.handle,
						(*C.float)(ptrTemp),
						nil,
						C.long(buffSize),
						&overflow,
						C.short(ch),
						C.short(centigrade),
						C.short(0)))

					if nvals < 0 {
						// Error
						log.WithFields(
							log.Fields{
								"device": device.ID,
							}).Error("Error while streaming, stopping")
						C.usb_tc08_stop(device.handle)
						device.running = false
						return
					} else if nvals > 0 {
						//Send the latest value as a struct
						newData := Measurement{
							Channel: device.channelNames[ch],
							Time:    time.Now(),
							Value:   tempBuf[nvals-1],
						}

						log.WithFields(
							log.Fields{
								"device": device.ID,
							}).Debug("DATA: ", newData)

						select { // Non-blocking send
						case device.Update <- newData:
							continue
						default:
							break
						}
					}
				}
			}

			if time.Now().After(device.overflowTime) {
				// This is a reminder that the timer overflow might cause issues,
				log.WithFields(log.Fields{
					"device": device.ID,
				}).Warn("Device time buffer reached max value")

				device.startTime = device.overflowTime
				device.overflowTime = device.startTime.Add(time.Duration(math.MaxInt32) * time.Millisecond)
			}
		}
	}
}

// SinlgeData Read data from the device
func (device *USBPicoDevice) SinlgeData() ([]float32, error) {
	if device.intialized {
		temps := make([]float32, int(C.usb_tc08_max_channels()+1))

		ptrTemps := unsafe.Pointer(&temps[0])

		if C.usb_tc08_get_single(device.handle, (*C.float)(ptrTemps), nil, C.short(0)) == 0 {
			return []float32{}, lastDeviceError("Failed getting temps", device.handle)
		}

		return temps, nil

	}

	return []float32{}, errors.New("Device uninitialized")
}

// CloseDevice Close and stop the H/W device
func (device *USBPicoDevice) CloseDevice() {
	if device.intialized {
		if device.running {
			log.WithFields(
				log.Fields{
					"device": device.ID,
				}).Info("Stopping", device.ID)
			// Device is doing a continuous read tell it to stop
			device.stopSignal <- true
			<-device.stopSignal // Wait for the closing to be done
			close(device.Update)
		}
		C.usb_tc08_close_unit(device.handle)

		device.intialized = false
		return
	}

	log.WithFields(log.Fields{
		"device": device.ID,
	}).Error("Closing uninitialised device")
}
