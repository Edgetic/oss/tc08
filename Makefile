.PHONY: clean tc08

MACHINE=$(shell uname -m)

ifeq ($(MACHINE),x86_64)
	LIB_ARCH=x86_64-linux-gnu
endif
ifeq ($(MACHINE),armv7l)
	LIB_ARCH=arm-linux-gnueabihf
endif
DRIVER_INC=/usr/include
DRIVER_LIB=/usr/lib/$(LIB_ARCH)/

tc08 : 
	CGO_CFLAGS="-I$(DRIVER_INC)" CGO_LDFLAGS="-L$(DRIVER_LIB)" go build 

clean :
	rm ./tc08
	rm ./go.sum